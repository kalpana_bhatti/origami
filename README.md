## Step to build install and use my project
1. Install Visual Studio Code 2019 for the source code.
2. Install .node and express to run the project.
3. Open Chrome Browser and open local host to run.
4. Database dump file is there in the folder. 
5. MongoDb is used in the backend for the database.

Name: Kalpana Bhatti

I added MIT licence in LICENCE.md because it was free to use.
